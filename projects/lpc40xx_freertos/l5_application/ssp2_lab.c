#include <stdio.h>
#include <stdint.h>
#include "ssp2_lab.h"
#include "clock.h"
#include "lpc40xx.h"
#include "ssp2.h"

static void ssp2_pin_init() {

  LPC_IOCON->P1_0 &= ~(0x7);
  LPC_IOCON->P1_0 |= (0x4);

  LPC_IOCON->P1_1 &= ~(0x7);
  LPC_IOCON->P1_1 |= (0x4);

  LPC_IOCON->P1_4 &= ~(0x7);
  LPC_IOCON->P1_4 |= 0x4;

  LPC_IOCON->P1_10 &= ~(0x7);
  LPC_GPIO1->DIR |= (1 << 10);
}

static void ssp2_enable_clock() { LPC_SC->PCONP |= (1 << 20); }

void scale_ssp2_peripheral_clock(uint32_t max_clock_speed_mhz) {
  uint32_t scale_factor = 2;
  uint32_t core_clock_speed_mhz = (clock__get_core_clock_hz() / 1000000);
  while (((core_clock_speed_mhz / scale_factor) > max_clock_speed_mhz) && (scale_factor < 255)) {
    scale_factor += 2;
  }
  if (scale_factor > 254) {
    scale_factor == 254;
  }
  LPC_SSP2->CPSR &= ~(0xFF);
  LPC_SSP2->CPSR |= (scale_factor & 0XFF);
}

void ssp2_set_control_registers() {
  LPC_SSP2->CR0 &= ~(0xF);
  LPC_SSP2->CR0 |= (0xF & 0x7);
  LPC_SSP2->CR1 |= (1 << 1);
}
static void ssp2_peripheral_init(uint32_t clock_speed) {
  ssp2_enable_clock();
  scale_ssp2_peripheral_clock(clock_speed);
  ssp2_set_control_registers();
}

void ssp2_lab__init(uint32_t max_clock_mhz) {
  ssp2_pin_init();
  ssp2_peripheral_init(max_clock_mhz);
}

uint8_t ssp2_lab__exchange_byte(uint8_t data_out) {
  uint8_t read_byte;

  LPC_SSP2->DR = data_out;
  while (LPC_SSP2->SR & (1<<4))
    ;
  read_byte = LPC_SSP2->DR & 0xFF;
  return read_byte;
}
