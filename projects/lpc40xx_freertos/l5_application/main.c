#include "i2c.h"
#include "i2c_slave_functions.h"
#include "i2c_slave_init.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "FreeRTOS.h"
#include <stdio.h>
#include "gpio.h"

#define MAX_INDEX 256
static volatile uint8_t slave_memory[MAX_INDEX];

bool i2c_slave_callback__read_memory(uint8_t memory_index, uint8_t *memory) {
  if (memory_index >= 256) {
    return false;
  }
  *memory = slave_memory[memory_index];
  return true;
}

bool i2c_slave_callback__write_memory(uint8_t memory_index, uint8_t memory_value) {
  if (memory_index >= 256) {
    return false;
  }
  slave_memory[memory_index] = memory_value;
  return true;
}

static void led_task(void *params) {
  while (1) {
    if (slave_memory[0] == 0) {
      gpio__set(board_io__get_led0());
    } else {
      gpio__reset(board_io__get_led0());
    }
    vTaskDelay(100);
  }
}

int main(void) {
  i2c2__slave_init(0x86);
  xTaskCreate(led_task, "led task", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  sj2_cli__init();
  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails
  return 0;
}

/*#include "FreeRTOS.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "queue.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "ssp2_lab.h"
#include "stdint.h"
#include <stdio.h>
#include "ff.h"

static QueueHandle_t sensor_queue;
static EventGroupHandle_t checkin;
static SemaphoreHandle_t mutex;

static void producer_task(void *p);
static void consumer_task(void *p);
static void watchdog_task(void *p);

static void write_file_using_fatfs_pi(int16_t value, const char *filename);
static void write_error_using_fatfs_pi(char error_message[], const char *filename);

static const EventBits_t bit_1 = (1 << 1);
static const EventBits_t bit_2 = (1 << 2);

int main(void) {
  sensor_queue = xQueueCreate(100, sizeof(int16_t));
  checkin = xEventGroupCreate();
  mutex = xSemaphoreCreateMutex();
  acceleration__init();
  xTaskCreate(producer_task, "producer", 2048 / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  xTaskCreate(consumer_task, "consumer", 2048 / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  xTaskCreate(watchdog_task, "watchdog", 2048 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  sj2_cli__init();
  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails
  return 0;
}

static void producer_task(void *p) {
  int16_t average_sensor_value = 0;
  int16_t sensor_values_sum = 0;
  size_t sensor_values_count = 0;

  while (1) {
    if (sensor_values_count < 100) {
      sensor_values_sum += acceleration__get_data().x;
      sensor_values_count++;
      vTaskDelay(1);
    } else {
      average_sensor_value = (sensor_values_sum / sensor_values_count);
      xQueueSend(sensor_queue, &average_sensor_value, 0);
      xEventGroupSetBits(checkin, bit_1);
      sensor_values_sum = 0;
      sensor_values_count = 0;
      vTaskDelay(100);
    }
  }
}
static void consumer_task(void *p) {
  int16_t average_sensor_value = 0;
  uint32_t time_elapsed = sys_time__get_uptime_ms();
  const char *filename = "accelerometer_x_axis_data.csv";

  while (1) {
    xQueueReceive(sensor_queue, &average_sensor_value, portMAX_DELAY);
    if (sys_time__get_uptime_ms() - time_elapsed > 1000) {
      if (xSemaphoreTake(mutex, 10) == pdTRUE) {
        write_file_using_fatfs_pi(average_sensor_value, filename);
        xSemaphoreGive(mutex);
      }
      time_elapsed = sys_time__get_uptime_ms();
    }
    xEventGroupSetBits(checkin, bit_2);
  }
}
static void watchdog_task(void *p) {
  const EventBits_t wait_for_bit_1_bit_2 = bit_1 | bit_2;
  const char *filename = "accelerometer_x_axis_data.csv";
  EventBits_t bits_set;

  while (1) {
    bits_set = xEventGroupWaitBits(checkin, wait_for_bit_1_bit_2, pdTRUE, pdFALSE, 200);

    if ((bits_set & (bit_1 | bit_2)) == (bit_1 | bit_2)) {
      printf("Check-in successful from both producer and consumer task\n");
    } else if ((bits_set & bit_1) != 0) {
      printf("Check-in successful from producer task\n");
      printf("ERROR: Consumer task failed to check-in\n");
      if (xSemaphoreTake(mutex, 10) == pdTRUE) {
        write_error_using_fatfs_pi("ERROR: Consumer task failed to check-in", filename);
        xSemaphoreGive(mutex);
      }
    } else if ((bits_set & bit_2) != 0) {
      printf("Check-in successful from consumer task\n");
      printf("ERROR: Producer task failed to check-in\n");
      if (xSemaphoreTake(mutex, 10) == pdTRUE) {
        write_error_using_fatfs_pi("ERROR: Producer task failed to check-in", filename);
        xSemaphoreGive(mutex);
      }
    } else {
      printf("ERROR: Producer and Consumer task failed to check-in within the 200ms threshold\n");
      printf("ERROR: Possibly due to consumer task blocked waiting on receiving as result of producer task failing to "
             "check-in\n");
      if (xSemaphoreTake(mutex, 10) == pdTRUE) {
        write_error_using_fatfs_pi("ERROR: Producer and Consumer task failed to check-in within the 200ms threshold",
                                   filename);
        write_error_using_fatfs_pi("ERROR: Possibly due to consumer task blocked waiting on receiving as result of "
                                   "producer task failing to check-in",
                                   filename);
        xSemaphoreGive(mutex);
      }
    }
    vTaskDelay(1000);
  }
}
static void write_file_using_fatfs_pi(int16_t value, const char *filename) {
  FIL file; // File handle
  UINT bytes_written = 0;
  FRESULT result = f_open(&file, filename, (FA_WRITE | FA_OPEN_APPEND));

  if (FR_OK == result) {
    char string[64];
    sprintf(string, "%li, %i\n", xTaskGetTickCount(), value);
    if (FR_OK == f_write(&file, string, strlen(string), &bytes_written)) {
    } else {
      printf("ERROR: Failed to write data to file\n");
    }
    f_close(&file);
  } else {
    printf("ERROR: Failed to open: %s\n", filename);
  }
}
static void write_error_using_fatfs_pi(char error_message[], const char *filename) {
  FIL file; // File handle
  UINT bytes_written = 0;
  FRESULT result = f_open(&file, filename, (FA_WRITE | FA_OPEN_APPEND));

  if (FR_OK == result) {
    char error_string[128];
    sprintf(error_string, "%li, %s \n", xTaskGetTickCount(), error_message);
    if (FR_OK == f_write(&file, error_string, strlen(error_string), &bytes_written)) {
    } else {
      printf("ERROR: Failed to write data to file\n");
    }
    f_close(&file);
  } else {
    printf("ERROR: Failed to open: %s\n", filename);
  }
}*/

/*#include "stdint.h"
#include "task.h"
#include "uart_lab.h"
#include <stdio.h>
#include "queue.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "FreeRTOS.h"
#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "semphr.h"
#include "sj2_cli.h"

static QueueHandle_t switch_queue;
typedef enum { switch__off, switch__on } switch_e;
switch_e get_switch_input_from_switch0(void) {
  if (LPC_GPIO0->PIN & (1 << 29))
    return switch__on;
  else
    return switch__off;
}

void producer(void *p) {
  while (1) {
    const switch_e switch_value = get_switch_input_from_switch0();
    printf("%s(), line %d, sending\n", __FUNCTION__, __LINE__);
    if (xQueueSend(switch_queue, &switch_value, 0)) {
      printf("%s(), line %d, sent\n", __FUNCTION__, __LINE__);
    }
    vTaskDelay(1000);
  }
}

void consumer(void *p) {
  switch_e switch_value;
  while (1) {
    printf("%s(), line %d, receiving\n", __FUNCTION__, __LINE__);
    if (xQueueReceive(switch_queue, &switch_value, 1000)) {
      printf("%s(), line %d, received\n", __FUNCTION__, __LINE__);
    }
    if (switch_value == switch__on) {
      printf("\nSwitch is PRESSED\n");
    } else {
      printf("\nSwitch is off\n");
    }
  }
}
int main(void) {
  switch_queue = xQueueCreate(1, sizeof(switch_e));
  xTaskCreate(producer, "PRODUCER", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(consumer, "CONSUMER", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails
  return 0;
}*/

/*#include "stdint.h"
#include "task.h"
#include "uart_lab.h"
#include <stdio.h>
#include "queue.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "FreeRTOS.h"

//part1
#ifdef part1
static void uart_read_task(void *p) {
  char rx_byte;
  while (1) {
    if (uart_lab__polled_get(UART_2, &rx_byte)) {
      printf("Recieved through polling %c \n", rx_byte);
    }
    vTaskDelay(1000);
  }
}
static void uart_write_task(void *p) {
  char to_send;
  while (1) {
    to_send = 'A';
    if (uart_lab__polled_put(UART_2, to_send)) {
      printf("Sending %c \n", to_send);
    }
    vTaskDelay(1000);
  }
}
int main(void) {
            
  uart_lab__init(UART_2, 96000000, 115200);
  xTaskCreate(uart_write_task, "uart_transmission", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(uart_read_task, "uart2_reception", 1024, NULL, PRIORITY_LOW, NULL);
  puts("Starting RTOS");
  vTaskStartScheduler(); 
  return 0;
}
#endif
//part1

//part2
#ifdef part2
static void uart_read_task_interrupt(void *p) {
  char rx_data;
  while (1) {
    uart_lab__get_char_from_queue(&rx_data, 100);
    printf("Recieved through interrupt %c \n", rx_data);
    vTaskDelay(2000);
  }
}
static void uart_write_task(void *p) {
  char to_send;
  while (1) {
    to_send = 'A';
    if (uart_lab__polled_put(UART_2, to_send)) {
      printf("Sending %c \n", to_send);
    }
    vTaskDelay(1000);
  }
}
int main(void) {
            
  uart_lab__init(UART_2, 96000000, 115200);
  uart__enable_receive_interrupt(UART_2);
  xTaskCreate(uart_write_task, "uart_transmission", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(uart_read_task_interrupt, "uart2_reception", 1024, NULL, PRIORITY_LOW, NULL);
  puts("Starting RTOS");
  vTaskStartScheduler(); 
  return 0;
}
#endif
//part2

//part3
#ifdef part3
void board_1_sender_task(void *p) {
  char number_as_string[16] = {0};

  while (true) {
    const int number = rand();
    sprintf(number_as_string, "%i", number);
    for (int i = 0; i <= strlen(number_as_string); i++) {
      uart_lab__polled_put(UART_2, number_as_string[i]);
      printf("Sent: %c\n", number_as_string[i]);
    }

    printf("Sent: %i over UART to the other board\n", number);
    vTaskDelay(3000);
  }
}

void board_2_receiver_task(void *p) {
  char number_as_string[16] = {0};
  int counter = 0;

  while (true) {
    char byte = 0;
    uart_lab__get_char_from_queue(&byte, portMAX_DELAY);
    printf("Received: %c\n", byte);
    if ('\0' == byte) {
      number_as_string[counter] = '\0';
      counter = 0;
      printf("Received this number from the other board: %s\n", number_as_string);
    }
    else {
      number_as_string[counter] = byte;
      counter++;
      if (counter == 16)
        counter = 0;
    }
  }
}
int main(void) {      
  uart_lab__init(UART_2, 96000000, 115200);
 // uart__enable_receive_interrupt(UART_2);
 // xTaskCreate(board_2_receiver_task, "read", 1000, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(board_1_sender_task, "send", 1000, NULL, PRIORITY_LOW, NULL);
  puts("Starting RTOS");
  vTaskStartScheduler(); 
  return 0;
}
#endif
//PART3*/
/*#include "semphr.h"
#include "sj2_cli.h"
#include "ssp2_lab.h"
#include "stdint.h"
#include "task.h"
#include <stdio.h>
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "FreeRTOS.h"


static void adesto_cs(void) { LPC_GPIO1->CLR = (1 << 10); }
static void adesto_ds(void) { LPC_GPIO1->SET = (1 << 10); }
typedef struct {
  uint8_t manufacturer_id;
  uint8_t device_id_1;
  uint8_t device_id_2;
  uint8_t extended_device_id;
} adesto_flash_id_s;
adesto_flash_id_s adesto_read_signature(void) {
  adesto_flash_id_s data = {0};
  adesto_cs();
  {
    (void)ssp2_lab__exchange_byte(0x9f);
    data.manufacturer_id = ssp2_lab__exchange_byte(0xFF);
    data.device_id_1 = ssp2_lab__exchange_byte(0xFF);
    data.device_id_2 = ssp2_lab__exchange_byte(0xFF);
    data.extended_device_id = ssp2_lab__exchange_byte(0xFF);
  }
  adesto_ds();
  return data;
}

//part1
#ifdef part1
void spi_task(void *p) {
  const uint32_t spi_clock_mhz = 24;
  ssp2__init(spi_clock_mhz);
  while (1) {
    adesto_flash_id_s id = adesto_read_signature();
    fprintf(stderr, "Manufacture Id: 0x%x \n", id.manufacturer_id);
    fprintf(stderr, "Device Id 1: 0x%x \n", id.device_id_1);
    fprintf(stderr, "Device Id 2: 0x%x \n", id.device_id_2);
    fprintf(stderr, "extended_device_id: 0x%x \n", id.extended_device_id);
    vTaskDelay(500);
  }
}
int main(void) {
  xTaskCreate(spi_task, "spi_task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails
  return 0;
}
#endif
//part1

//part2
#ifdef part2
static SemaphoreHandle_t spi_task_mutex;
void spi_task(void *p) {
  const uint32_t spi_clock_mhz = 24;
  ssp2__init(spi_clock_mhz);
  while (1) {
    xSemaphoreTake(spi_task_mutex, portMAX_DELAY);
    adesto_flash_id_s id = adesto_read_signature();
    fprintf(stderr,"Manufacturer ID: 0x%x\nDevice ID1: 0x%x\nDevice ID2: 0x%x\n", id.manufacturer_id, id.device_id_1,id.device_id_2);
    xSemaphoreGive(spi_task_mutex);
    if (id.manufacturer_id != 0x1F) {
      fprintf(stderr, "Error reading the manufacturer ID expected result 0x1F, got 0x%x\n", id.manufacturer_id);
    }
    vTaskDelay(500);
  }
}
int main(void) {
  spi_task_mutex = xSemaphoreCreateMutex();
  xTaskCreate(spi_task, "spi_task1", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(spi_task, "spi_task2", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails
  return 0;
}
#endif
//part2 */

/*#include<stdio.h>
#include "FreeRTOS.h"
#include "task.h"
#include "gpio.h"
#include "pwm1.h"
#include "adc.h"
#include "board_io.h"
//part1
#ifdef part0
void pin_configure_pwm_channel_as_io_pin() { gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCTION_1); }
static void pwm_task(void *p) {
  pwm1__init_single_edge(1000);
  pin_configure_pwm_channel_as_io_pin();
  pwm1__set_duty_cycle(PWM1__2_0, 50);
  uint8_t percent = 0;
  while (1) {
    pwm1__set_duty_cycle(PWM1__2_0, percent);
    fprintf(stderr, "MR0=%lu  MR1=%lu\n", LPC_PWM1->MR0, LPC_PWM1->MR1);
    if (++percent > 100) {
      percent = 0;
    }
    vTaskDelay(100);
  }
}
int main(void) {
  xTaskCreate(pwm_task, "pwm_task", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler(); 
  return 0;
}
#endif
//part0

//part1
#ifdef part1
void pin_configure_adc_channel_as_io_pin(adc_channel_e channel_number, gpio__function_e function) {
  const uint32_t func_mask = UINT32_C(7);
  LPC_IOCON->P1_31 &= ~(func_mask << 0);
  LPC_IOCON->P1_31 |= ((uint32_t)function & func_mask);
  LPC_IOCON->P1_31 &= ~((1 << 7) | (1 << 4) | (1 << 3));
}
static void adc_task(void *p) {
  adc__initialize();
  adc__enable_burst_mode();
  pin_configure_adc_channel_as_io_pin(ADC__CHANNEL_5, GPIO__FUNCTION_3);
  while (1) {
    const uint16_t adc_reading = adc__get_channel_reading_with_burst_mode(ADC__CHANNEL_5);
    const float adc_volatge = (adc_reading * 3.3) / 4095.0;
    fprintf(stderr, "ADC_Reading=%04d ADC_Voltage=%0.1fV \n", adc_reading, adc_volatge);
    vTaskDelay(100);
  }
}
int main(void) {
  xTaskCreate(adc_task, "adc_task", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
  return 0;
}
#endif
//part1

//part2&3
#ifdef part2_3
#include "queue.h"
static QueueHandle_t adc_to_pwm_task_queue;

void pin_configure_pwm_channel_as_io_pin() {
  gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCTION_1);
  gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCTION_1);
  gpio__construct_with_function(GPIO__PORT_2, 2, GPIO__FUNCTION_1);
}
static void pwm_task(void *p) {
  pwm1__init_single_edge(1000);
  pin_configure_pwm_channel_as_io_pin();
  uint16_t adc_reading = 0;
  while (1) {
    if (xQueueReceive(adc_to_pwm_task_queue, &adc_reading, 100)) {
      float voltage = 3.3 * ((float)(adc_reading) / 4095);
      fprintf(stderr, "PWM voltage is %f \n", voltage);
      if (adc_reading < 1365) {
        pwm1__set_duty_cycle(PWM1__2_0, (adc_reading * 100) / 1365);
        pwm1__set_duty_cycle(PWM1__2_1, 0);
        pwm1__set_duty_cycle(PWM1__2_2, 0);
      } else if (adc_reading >= 1365 && adc_reading < 2730) {
        pwm1__set_duty_cycle(PWM1__2_1, ((adc_reading - 1365) * 100) / 1365);
        pwm1__set_duty_cycle(PWM1__2_0, 0);
        pwm1__set_duty_cycle(PWM1__2_2, 0);
      } else if (adc_reading > 2730) {
        pwm1__set_duty_cycle(PWM1__2_2, ((adc_reading - 2730) * 100) / 1365);
        pwm1__set_duty_cycle(PWM1__2_0, 0);
        pwm1__set_duty_cycle(PWM1__2_1, 0);
      }
    }
  }
}
void pin_configure_adc_channel_as_io_pin(adc_channel_e channel_number, gpio__function_e function) {
  const uint32_t func_mask = UINT32_C(7);
  LPC_IOCON->P1_31 &= ~(func_mask << 0);
  LPC_IOCON->P1_31 |= ((uint32_t)function & func_mask);
  LPC_IOCON->P1_31 &= ~((1 << 7) | (1 << 4) | (1 << 3));
}
static void adc_task(void *p) {
  adc__initialize();
  adc__enable_burst_mode();
  pin_configure_adc_channel_as_io_pin(ADC__CHANNEL_5, GPIO__FUNCTION_3);
  while (1) {
    const uint16_t adc_reading = adc__get_channel_reading_with_burst_mode(ADC__CHANNEL_5);
    xQueueSend(adc_to_pwm_task_queue, &adc_reading, 0);
    fprintf(stderr, "ADC reading value is: %d \n", adc_reading);
    vTaskDelay(1000);
  }
}

int main(void) {
  adc_to_pwm_task_queue = xQueueCreate(1, sizeof(int));
  xTaskCreate(adc_task, "adc_task", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(pwm_task, "pwm_task", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler(); 
  return 0;
}
#endif
//part2&3

/*#include "FreeRTOS.h"
#include "lpc_peripherals.h"
#include <stdio.h>
void delay__ms(uint32_t ms);
//part0
#ifdef part0
void gpio_interrupt_part0(void) {
  fprintf(stderr, "Main Interrupted\n");
  LPC_GPIOINT->IO0IntClr |= (1 << 29);
}
int main(void){
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio_interrupt_part0, "part0");
    LPC_GPIO0->DIR &= ~(1 << 29);        
    LPC_GPIOINT->IO0IntEnR |= (1 << 29); 
    NVIC_EnableIRQ(GPIO_IRQn);
    LPC_GPIO1->DIR |= (1 << 24);
    while (1) {
      delay__ms(100);
      LPC_GPIO1->PIN |= (1 << 24);
      delay__ms(100);
      LPC_GPIO1->PIN &= ~(1 << 24);
      delay__ms(100);
      fprintf(stderr, "Main Running\n");
     }
  return 0;
}
#endif
//part0

//part1
#ifdef part1
#include "semphr.h"
#include "task.h"
static SemaphoreHandle_t switch_pressed_signal;
void configure_your_gpio_interrupt() {
  LPC_GPIO0->DIR &= ~(1 << 29);
  LPC_GPIOINT->IO0IntEnR |= (1 << 29);
}
void clear_gpio_interrupt() { LPC_GPIOINT->IO0IntClr |= (1 << 29); }
void gpio0__interrupt_part1(void) {
  fprintf(stderr, "ISR Entry: Give Semaphore\n");
  xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
  clear_gpio_interrupt();
}
void sleep_on_sem_task(void *p) {
  LPC_GPIO1->DIR |= 1 << 24; 
  while (1) {
    // Use xSemaphoreTake with forever delay and blink an LED when you get the signal
    if (xSemaphoreTake(switch_pressed_signal, portMAX_DELAY)) {
      fprintf(stderr, "semaphore Taken \n");
      LPC_GPIO1->PIN |= 1 << 24;
      delay__ms(200);
      LPC_GPIO1->PIN &= ~(1 << 24);
      delay__ms(200);
    }
  }
}
int main(void){
  switch_pressed_signal = xSemaphoreCreateBinary();
  configure_your_gpio_interrupt();
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio0__interrupt_part1, "part1");
  NVIC_EnableIRQ(GPIO_IRQn);
  xTaskCreate(sleep_on_sem_task, "sem", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  return 0;
}
#endif
//part1

//part2
#ifdef part2
#include "gpio_isr.h"
int main(void){
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio0__interrupt_part2, "part2");
  gpio0__attach_interrupt(29, GPIO_INTR__FALLING_EDGE, pin29_isr);
  gpio0__attach_interrupt(30, GPIO_INTR__RISING_EDGE, pin30_isr);
  NVIC_EnableIRQ(GPIO_IRQn);
  while (1) {
    fprintf(stderr, "Main Routine\n");
    delay__ms(1000);
  }
  return 0;
}
#endif
//part2*/



/*#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

#include "gpio_lab.h"
#include "lpc40xx.h"
#include "semphr.h"
#include "stdint.h"

// 'static' to make these functions 'private' to this file
static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);

static void task_one(void *task_parameter);
static void task_two(void *task_parameter);

typedef struct {
  uint8_t pin;
} port_pin_s;

#ifdef lab_part2
void led_task_part2(void *task_parameter) {
  const port_pin_s *led = (port_pin_s *)(task_parameter);
  gpio1__set_as_output(led->pin);
  while (true) {
    gpio1__set_high(led->pin);
    vTaskDelay(1000);
    gpio1__set_low(led->pin);
    vTaskDelay(1000);
  }
}
#endif

#ifdef lab_part3
static SemaphoreHandle_t switch_press_indication;

void led_task_part3(void *task_parameter) {
  port_pin_s *led_pin = (port_pin_s *)task_parameter;
  gpio1__set_as_output(led_pin->pin); 
  while (true) {
    if (xSemaphoreTake(switch_press_indication, 100)) {// Note: There is no vTaskDelay() here, but we use sleep mechanism while waiting for the binary semaphore (signal)
      gpio1__set_low(led_pin->pin); active high -> LED ON
      vTaskDelay(500);
      gpio1__set_high(led_pin->pin); active low -> LED OFF
    } else {
      puts("Timeout: No switch press indication for 1000ms");
    }
  }
}

void switch_task(void *task_parameter) {
  port_pin_s *switch_pin = (port_pin_s *)task_parameter;
  gpio1__set_as_input(switch_pin->pin);
  while (true) {
    if (gpio1__get_level(switch_pin->pin)) { 
      xSemaphoreGive(switch_press_indication);
    }
    vTaskDelay(100);
  }
}
#endif

int main(void) {
  create_blinky_tasks();
  create_uart_task();

  // If you have the ESP32 wifi module soldered on the board, you can try uncommenting this code
  // See esp32/README.md for more details
  // uart3_init();                                                                     // Also include:  uart3_init.h
  // xTaskCreate(esp32_tcp_hello_world_task, "uart3", 1000, NULL, PRIORITY_LOW, NULL); // Include esp32_task.h

 #ifdef lab_part2
  static port_pin_s led0 = {18};
  static port_pin_s led1 = {24};
  xTaskCreate(led_task_part2, "led blinking", 2048 / sizeof(void *), &led0, PRIORITY_LOW, NULL);
  xTaskCreate(led_task_part2, "led blinking", 2048 / sizeof(void *), &led1, PRIORITY_LOW, NULL);
#endif

#ifdef lab_part3
 static port_pin_s switch_port = {15};
 static port_pin_s led_port = {24};
  switch_press_indication = xSemaphoreCreateBinary();
  xTaskCreate(led_task, "led", 2048 / sizeof(void *), &led_port, PRIORITY_LOW, NULL);
  xTaskCreate(switch_task, "switch", 2048 / sizeof(void *), &switch_port, PRIORITY_LOW, NULL);
#endif

  puts("Starting RTOS");

  xTaskCreate(task_one, "Task 1", 4096 / sizeof(void *), NULL, 1, NULL);
  xTaskCreate(task_two, "Task 2", 4096 / sizeof(void *), NULL, 2, NULL);

  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}

static void task_one(void *task_parameter) {
  while (true) {
    fprintf(stderr, "AAAAAAAAA");
    vTaskDelay(1000);
  }
}

static void task_two(void *task_parameter) {
  while (true) {
    fprintf(stderr, "BBBBBBBBB");
    vTaskDelay(1000);
  }
}


static void create_blinky_tasks(void) {
  /**
   * Use '#if (1)' if you wish to observe how two tasks can blink LEDs
   * Use '#if (0)' if you wish to use the 'periodic_scheduler.h' that will spawn 4 periodic tasks, one for each LED
   */
#if (1)
  // These variables should not go out of scope because the 'blink_task' will reference this memory
  static gpio_s led0, led1;

  // If you wish to avoid malloc, use xTaskCreateStatic() in place of xTaskCreate()
  static StackType_t led0_task_stack[512 / sizeof(StackType_t)];
  static StackType_t led1_task_stack[512 / sizeof(StackType_t)];
  static StaticTask_t led0_task_struct;
  static StaticTask_t led1_task_struct;

  led0 = board_io__get_led0();
  led1 = board_io__get_led1();

  xTaskCreateStatic(blink_task, "led0", ARRAY_SIZE(led0_task_stack), (void *)&led0, PRIORITY_LOW, led0_task_stack,
                    &led0_task_struct);
  xTaskCreateStatic(blink_task, "led1", ARRAY_SIZE(led1_task_stack), (void *)&led1, PRIORITY_LOW, led1_task_stack,
                    &led1_task_struct);
#else
  periodic_scheduler__initialize();
  UNUSED(blink_task);
#endif
}

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

static void blink_task(void *params) {
  const gpio_s led = *((gpio_s *)params); // Parameter was input while calling xTaskCreate()

  // Warning: This task starts with very minimal stack, so do not use printf() API here to avoid stack overflow
  while (true) {
    gpio__toggle(led);
    vTaskDelay(500);
  }
}

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}*/
