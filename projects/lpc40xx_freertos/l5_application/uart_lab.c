#include "uart_lab.h"
#include "FreeRTOS.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "queue.h"

static QueueHandle_t your_uart_rx_queue;

static void set_uart_ports(uart_number_e uart) {
  uint32_t mask = 0x7;
  if (uart == UART_2) {
    LPC_IOCON->P2_8 &= ~(mask);
    LPC_IOCON->P2_8 |= (mask & 2);
    LPC_IOCON->P2_9 &= ~(mask);
    LPC_IOCON->P2_9 |= (mask & 2);
  } else if (uart == UART_3) {
    LPC_IOCON->P2_8 &= ~(mask);
    LPC_IOCON->P2_8 |= (mask & 2);
    LPC_IOCON->P2_9 &= ~(mask);
    LPC_IOCON->P2_9 |= (mask & 2);
  }
}

static void power_on_uart(uart_number_e uart) {
  if (uart == UART_2) {
    LPC_SC->PCONP |= (1 << 24);
  } else if (uart == UART_3) {
    LPC_SC->PCONP |= (1 << 25);
  }
  return;
}

void set_divider_value(uart_number_e uart, uint32_t divider_value) {
  LPC_UART_TypeDef *uart_reg;
  uint32_t dlab_bit = (1 << 7);
  uint32_t set_eight_bit_mode = 0x3;
  if (uart == UART_2) {
    uart_reg = LPC_UART2;
  } else if (uart == UART_3) {
    uart_reg = LPC_UART3;
  }
  uart_reg->LCR |= dlab_bit;
  uart_reg->LCR |= set_eight_bit_mode;
  uart_reg->DLL = divider_value & 0xFF;
  uart_reg->DLM = ((divider_value >> 8) & 0xFF);
  uart_reg->FCR |= (1 << 0);
  uart_reg->LCR &= ~(dlab_bit);
  return;
}

void uart_lab__init(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate) {
  uint32_t divider_value;
  set_uart_ports(uart);
  power_on_uart(uart);
  divider_value = peripheral_clock / (16 * baud_rate);
  set_divider_value(uart, divider_value);
}

bool uart_lab__polled_get(uart_number_e uart, char *input_byte) {
  uint32_t rdr_bit = (1 << 0);
  LPC_UART_TypeDef *uart_reg;
  if (uart == UART_2) {
    uart_reg = LPC_UART2;
  } else if (uart == UART_3) {
    uart_reg = LPC_UART3;
  }
  while (!(uart_reg->LSR & rdr_bit))
    ;
  *input_byte = uart_reg->RBR;
  return 1;
}

bool uart_lab__polled_put(uart_number_e uart, char output_byte) {
  uint32_t thre_bit = (1 << 5);
  LPC_UART_TypeDef *uart_reg;
  if (uart == UART_2) {
    uart_reg = LPC_UART2;
  } else if (uart == UART_3) {
    uart_reg = LPC_UART3;
  }
  while (!(uart_reg->LSR & thre_bit))
    ;
  uart_reg->THR = output_byte;
  return 1;
}

static void your_receive_interrupt(void) {
  char input_byte = 0;
  uart_lab__polled_get(UART_2, &input_byte);
  xQueueSendFromISR(your_uart_rx_queue, &input_byte, NULL);
}

void uart__enable_receive_interrupt(uart_number_e uart_number) {
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART2, your_receive_interrupt, "UART_INTR");
  LPC_UART2->IER = 1;
  your_uart_rx_queue = xQueueCreate(10, sizeof(char));
}

bool uart_lab__get_char_from_queue(char *input_byte, uint32_t timeout) {
  return xQueueReceive(your_uart_rx_queue, input_byte, timeout);
}
