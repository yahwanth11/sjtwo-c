#include "i2c_slave_init.h"
#include "lpc40xx.h"

void i2c2__slave_init(uint8_t slave_address_to_respond_to) {
  const uint8_t slave_mode = ((1 << 2) | (1 << 6));

  LPC_I2C2->CONSET = slave_mode;
  LPC_I2C2->ADR0 = slave_address_to_respond_to;
  LPC_I2C2->MASK0 = 0;
}
