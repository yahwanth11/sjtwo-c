#include "gpio_isr.h"
#include "FreeRTOS.h"
#include "lpc40xx.h"
#include <stdint.h>
#include <stdio.h>

static function_pointer_t gpio0_callbacks[32] = {NULL};
void delay__ms(uint32_t ms);
void pin29_isr(void) {
  LPC_GPIO1->PIN |= 1 << 24;
  delay__ms(200);
  LPC_GPIO1->PIN &= ~(1 << 24);
  delay__ms(200);
  fprintf(stderr, "PIN 29 ISR, Falling edge \n");
}

void pin30_isr(void) {
  LPC_GPIO1->PIN |= 1 << 26;
  delay__ms(200);
  LPC_GPIO1->PIN &= ~(1 << 26);
  delay__ms(200);
  fprintf(stderr, "PIN 30 ISR, Rising edge\n");
}

void gpio0__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback) {
  LPC_GPIO0->DIR &= ~(1 << pin);
  switch (interrupt_type) {
  case GPIO_INTR__FALLING_EDGE:
    LPC_GPIOINT->IO0IntEnF |= (1 << pin);
    break;
  case GPIO_INTR__RISING_EDGE:
    LPC_GPIOINT->IO0IntEnR |= (1 << pin);
    break;
  default:
    break;
  }
  gpio0_callbacks[pin] = callback;
}

int get_interrupt_pin_number() {
  unsigned int p = 0;
  if (LPC_GPIOINT->IO0IntStatR)
    p = LPC_GPIOINT->IO0IntStatR;
  else if (LPC_GPIOINT->IO0IntStatF)
    p = LPC_GPIOINT->IO0IntStatF;
  int i = 0;
  while (p) {
    p = p >> 1;
    i++;
  }
  fprintf(stderr, "Interrupt on pin %d\n", i - 1);
  return (i - 1);
}

void clear_pin_interrupt(int pin_that_generated_interrupt) {
  LPC_GPIOINT->IO0IntClr |= (1 << pin_that_generated_interrupt);
}

void gpio0__interrupt_part2(void) {
  int pin_that_generated_interrupt = get_interrupt_pin_number();
  if (pin_that_generated_interrupt == -1) {
    fprintf(stderr, "Wrong Interrupt\n");
    return;
  }
  function_pointer_t attached_user_handler = gpio0_callbacks[pin_that_generated_interrupt];
  if (attached_user_handler == NULL) {
    fprintf(stderr, "Nothing is attached\n");
  }

  // Invoke the user registered callback, and then clear the interrupt
  attached_user_handler();
  clear_pin_interrupt(pin_that_generated_interrupt);
}
